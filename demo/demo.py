#!/usr/bin/env python
"""
This is just a demo of how to run the postprocessing script.
Please fill in the correct paths.

Have fun!
"""
# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of PULSE-ADJOINT.
#
# PULSE-ADJOINT is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PULSE-ADJOINT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PULSE-ADJOINT. If not, see <http://www.gnu.org/licenses/>.
import pulse_adjoint_post as post
from pulse_adjoint.setup_optimization import setup_adjoint_contraction_parameters, setup_general_parameters
import os

valvular_events = {'patient1': {'echo_valve_time': {'avc': 8,
                                          'avo': 0,
                                          'end': 27,
                                          'mvc': 0,
                                          'mvo': 10},
                      'passive_filling_begins': 25,
                      'passive_filling_duration': 3},
        'patient2': {'echo_valve_time': {'avc': 8,
                                         'avo': 0,
                                         'end': 26,
                                         'mvc': 27,
                                         'mvo': 9},
                     'passive_filling_begins': 25,
                     'passive_filling_duration': 3}}
}

healthy_patients = ["patient1"]
lbbb_patients = ["patient2"]

curdir = os.path.dirname(os.path.abspath(__file__))
resdir = "/".join([curdir, "/path/to/the/results/{patient}"])
figdir = "/".join([curdir, "/path/to/where/you/want/to/save/the/figures"])
mesh_path =   "path/to/your/mesh/data/{patient}.h5"
pressure_path =  "path/to/your/pressure/data/{patient}.yml"
fname = "/".join([curdir, "path/to/where/to/save/the/extracted/results.h5"])
active_model = "active_strain"
gamma_space = "CG_1"


def main():

    recompute = False
    
    patient_names = healthy_patients + lbbb_patients
    

    params = setup_adjoint_contraction_parameters()
    resolution = "low_res"
    params["Patient_parameters"]["resolution"] = resolution
    params["Patient_parameters"]["mesh_type"] = "lv"
    params["Patient_parameters"]["fiber_angle_epi"] = -60
    params["Patient_parameters"]["fiber_angle_endo"] = 60
    params["active_model"] = active_model
    params["gamma_space"] = gamma_space
    outdir = figdir.format(wild, active_model, gamma_space)
    

    if os.path.isfile(fname):

        res = post.PostProcess(fname, outdir,
                               mesh_path, pressure_path,
                               params, simdir,
                               patient_names,
                               valvular_events,
                               recompute = recompute)

    else:
        res = {p:{} for p in patient_names}
        setup_general_parameters()
        
        for p in patient_names:

            
            params["Patient_parameters"]["patient"] = p
            params["Patient_parameters"]["mesh_path"] = mesh_path.format(p)
            params["Patient_parameters"]["pressure_path"] = pressure_path.format(p)
            
            outdir = simdir.format(p)
            params["sim_file"] = "/".join([outdir,"result.h5"])
            params["outdir"] = outdir

            patient = post.load.initialize_patient_data(params["Patient_parameters"], False)

            res[p] = post.load.get_data(params, patient)
        
        post.load.save_dict_to_h5(res, fname)
        res = post.PostProcess(fname, outdir,
                               mesh_path, pressure_path,
                               params, simdir,valvular_events)


    
    
    
    
    lst = ["volume", "strain", "gamma_mean",
           "gamma_regional", "emax",
           "geometric_distance",
           "time_varying_elastance",
           "data_mismatch"]
    
    res.compute(*lst)
   
    
    res.plot_pv_loop(True)
    res.plot_strain()
    res.plot_gamma("both")
    res.plot_gamma_synch("both", {"Healthy":healthy_patients, "LBBB": lbbb_patients})
    res.plot_strain_scatter(split = 1)
    res.plot_volume_scatter()
    
    res.plot_emax({"Healthy":healthy_patients, "LBBB": lbbb_patients})
    res.plot_time_varying_elastance()
    res.plot_gamma_synch_mean("mean", {"Healthy":healthy_patients, "LBBB": lbbb_patients})

    res.print_data_mismatch_table()
    res.print_geometric_distance_table()
    
if __name__ == "__main__":
    main()
