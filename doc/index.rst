.. Pulse-Adjoint Postprocessing documentation master file, created by
   sphinx-quickstart on Wed Nov 30 10:13:17 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Pulse-Adjoint Postprocessing's documentation!
========================================================

pulse_adjoint_post is developed to simplify the postprocessing results, produced by pulse_adjoin

pulse_adjoint and all its extensions are developed at `Simula Research Laboratory <https://www.simula.no/>`_
and funded by the `Center for Cardiological Innovation
<http://heart-sfi.no>`__, a Norwegian Centre of Research Based Innovation.


Installation and dependencies:
------------------------------

The pulse-adjoint source code is hosted on Bitbucket:

  https://bitbucket.org/finsberg/pulse_adjoint

pulse_adjoint is based on

* The FEniCS Project software (http://www.fenicsproject.org)
* dolfin-adjoint (http://www.dolfin-adjoint.org)

See the separate file ./INSTALL in the root directory of your cbcbeat
source for a complete list of dependencies.

Main authors:
-------------

  * Henrik Finsberg    (henriknf@simula.no)
    

API documentation:
------------------

.. toctree::
   :maxdepth: 2

   pulse_adjoint_post
   modules
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

